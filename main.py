import time
import logging

from app import bot

logger = logging.getLogger('main_logger')


def launch(restart_timer=5):
    while 1:
        try:
            bot.polling(none_stop=True)
        except Exception as err:
            logger.error(
                f'An error occurred during startup! Restarting the bot after {restart_timer} seconds.',
                exc_info=err
            )
            time.sleep(restart_timer)


if __name__ == '__main__':
    launch(restart_timer=20)
