# TelegaBot

The goal of this project is to gradually expand the capabilities of the bot for working with incoming and outgoing messages. At the moment, his tasks include:

* Convert incoming audio messages to wav format with a sampling frequency of 16kHz.
* Save converted audio messages on the server and write its to the database by user IDs.
* Save photos with faces on the server.
***
The bot is supported by the following libraries:

Title                   | Description
------------------------|----------------------
pyTelegramBotAPI        | _The basis of the project_
ffmpy (based on ffmpeg) | _Processing sound files_
opencv-python           | _Image processing_
psycopg2                | _Interaction with DBMS PostgreSQL_
SQLAlchemy              | _Interacting with a database using ORM_

> **Note:** to use ffmpy, you must install ffmpeg.