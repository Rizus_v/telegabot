import logging.config

from config import ConfigBot


def init_logger():
    logging.config.dictConfig(ConfigBot.LOGGER_CONFIG)