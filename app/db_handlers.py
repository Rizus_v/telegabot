from sqlalchemy.orm import sessionmaker

from .db_models import engine, Teleuser, Audio

Session = sessionmaker()
Session.configure(bind=engine)
session = Session()


def save_in_db(user_id, audio_id):
    user = get_user(user_id)
    num = check_audio_num(user_id)
    audio_name = f'audio_message_{num}'
    new_audio = Audio(id=audio_id, name=audio_name)
    user.audio_id.append(new_audio)
    session.add(new_audio)
    session.commit()


def check_audio_num(user_id):
    audio_list = session.query(Teleuser).filter_by(id=user_id).one().audio_id
    num = len(audio_list)
    return num


def get_user(user_id):
    user = session.query(Teleuser).filter_by(id=user_id).first()
    if not user:
        user = add_user(user_id)
    return user


def add_user(user_id):
    new_user = Teleuser(id=user_id)
    session.add(new_user)
    session.commit()
    return new_user