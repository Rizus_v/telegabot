import os
import os.path as pth
import logging

import telebot

from config import ConfigBot
from .logger import init_logger

init_logger()

from .converter import convert_audio
from .db_handlers import save_in_db
from .photo_handlers import photo_handler

telebot.apihelper.proxy = {
    'https': ConfigBot.PROXY_URI
}
bot = telebot.TeleBot(ConfigBot.BOT_TOKEN, threaded=False)

logger = logging.getLogger('main_logger')


@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'OOkey, it\'s me')
    user_id = message.from_user.id
    logger.info(f"Dialogue with user-{user_id} was started")


@bot.message_handler(content_types=['voice'])
def get_audio(message):

    audio_id = message.voice.file_id
    user_id = message.from_user.id
    logger.info(f"Audio message {audio_id} from user {user_id} was received")
    file_info = bot.get_file(audio_id)
    download_file = bot.download_file(file_info.file_path)

    audio_path = ConfigBot.AUDIO_PATH or pth.join(
        pth.abspath(pth.dirname(__file__)), 'audio'
    )
    audio_full_name = pth.join(audio_path, f'{audio_id}.ogg')

    with open(audio_full_name, 'wb') as new_file:
        new_file.write(download_file)

    convert_audio(audio_full_name)
    save_in_db(user_id, audio_id)
    logger.info(f"Operation with the audio {audio_id} has been completed")


@bot.message_handler(content_types=['photo'])
def get_image(message):
    photo_id = message.photo.pop().file_id
    user_id = message.from_user.id
    logger.info(f"Photo message {photo_id} from user {user_id} was received")
    file_info = bot.get_file(photo_id)
    download_file = bot.download_file(file_info.file_path)

    photo_path = ConfigBot.PHOTO_PATH or pth.join(
        pth.abspath(pth.dirname(__file__)), 'photo'
    )
    photo_full_name = pth.join(photo_path, f'{photo_id}.jpg')
    with open(photo_full_name, 'wb') as new_file:
        new_file.write(download_file)

    photo_handler(photo_full_name)
    logger.info(f"Operation with the photo {photo_id} has been completed")


if __name__ == '__main__':
    bot.polling(none_stop=True, timeout=100)
