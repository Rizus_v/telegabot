import os

import ffmpy


def convert_audio(audio_full_name):
    without_format, _ = audio_full_name.split('.')
    ff = ffmpy.FFmpeg(
        inputs={audio_full_name: None},
        outputs={f'{without_format}.wav': '-ar 16000'}
    )
    ff.run()

    os.remove(audio_full_name)