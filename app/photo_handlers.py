import os
import os.path as pth
import logging

import cv2

from config import ConfigBot

logger = logging.getLogger('main_logger')


def photo_handler(photo_full_name):
    photo = cv2.imread(photo_full_name)

    face_cascade_path = ConfigBot.FACECASCADE or pth.join(
        cv2.__file__.split('cv2.cpython').pop(0), 'data', 'haarcascade_frontalface_default.xml'
    )
    face_cascade = cv2.CascadeClassifier(face_cascade_path)
    gray = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.3,
        minNeighbors=5,
        minSize=(10, 10)
    )
    if not len(faces):
        os.remove(photo_full_name)
        logger.info(f"There are no faces in the photo {photo_full_name}: wasn't saved!")