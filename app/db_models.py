import logging

from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from config import ConfigBot

logger = logging.getLogger('main_logger')

engine = create_engine(
    ConfigBot.SQLALCHEMY_DATABASE_URI, echo=ConfigBot.SQLALCHEMY_ECHO
)
Model = declarative_base()


class Teleuser(Model):
    __tablename__ = 'teleuser'
    id = Column(Integer, primary_key=True)
    audio_id = relationship("Audio", backref='teleuser')


class Audio(Model):
    __tablename__ = 'audio'
    id = Column(String, primary_key=True)
    name = Column(String, nullable=False)
    teleuser_id = Column(Integer, ForeignKey('teleuser.id'))


Model.metadata.create_all(bind=engine)
logger.info("Create DB table")