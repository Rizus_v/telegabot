class ConfigBot(object):
    PROXY_URI = 'socks5://139.99.104.233:36012'
    BOT_TOKEN = '1097689114:AAHR-Xy7sLkRpN4BOC5l8jZbRCePic_nqzs'

    POSTGRES_USER = 'mainuser'
    POSTGRES_PASSWORD = 'mainuser'
    POSTGRES_URL = 'localhost'
    POSTGRESS_DB = 'nurschool'

    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}'.format(
        user=POSTGRES_USER,
        pw=POSTGRES_PASSWORD,
        url=POSTGRES_URL,
        db=POSTGRESS_DB
    )
    SQLALCHEMY_ECHO = True

    FACECASCADE = None

    AUDIO_PATH = None
    PHOTO_PATH = None

    LOGGER_CONFIG = {
        "version": 1,
        "handlers": {
            "stream_handler": {
                "class": 'logging.StreamHandler',
                "formatter": 'format_1',
                "level": 'INFO',
                "stream": 'ext://sys.stdout'
            },
            "file_handler": {
                "class": 'logging.FileHandler',
                "formatter": 'format_1',
                "level": 'INFO',
                "filename": 'app.log'
            }
        },
        "loggers": {
            "main_logger": {
                "handlers": ["stream_handler"],
                "level": 'INFO'
            },
            "file_logger": {
                "handlers": ["file_handler"],
                "level": 'INFO'
            }
        },
        "formatters": {
            "format_1": {
                "format": '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
            }
        }
    }


